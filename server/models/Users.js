const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
	first_name: {
		type: String,
		required: true,
	},
	last_name: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	birth_date: {
		type: String,
	},
	gender: {
		type: String,
		enum: ['M','F','O'],
	},
	photo: {
		type: String,
	},
	events_created: {
		type: [Schema.Types.ObjectId],
		ref: 'events',
	},
	events_assists: {
		type: [Schema.Types.ObjectId],
		ref: 'events',
	},
	is_active: {
		type: Boolean,
		default: true,
	}
}, {
	timestamps: true
});

UsersSchema.pre('save', function(next){
	const user = this;
	if(!user.isModified('password')) next();
	
	const SALT_FACTOR = parseInt(process.env.SALT_FACTOR);
	bcrypt.genSalt(SALT_FACTOR, function(error, salt){
		if(error) return next(error);
		bcrypt.hash(user.password, salt, function(err, hash) {
			user.password = hash;
			next();
		});
	});
});

module.exports = mongoose.model('users', UsersSchema);