const Users = require('../../models/Users');
const authenticate = require('../../utils/authenticate');

module.exports = {
	getUsers: (root, args) => {
		return Users.find(args).exec();
	},
	getUser: (root, args) => {
		if(args.email && args.id) return Users.find({ email: args.email, _id:args.id }).exec();
		if(args.email) return Users.findOne({ email: args.email }).exec();
		if(args.id) return Users.findOne(args.id).exec();
		return new Error('You must pass one parameter');
	},
	login: (root, args) => {
		return authenticate(args.email, args.password);
	}
};