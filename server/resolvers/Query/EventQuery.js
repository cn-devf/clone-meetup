const Events = require('../../models/Events');

module.exports = {
	getEvents: (root, args) => {
		const populateField = 'created_by';
		if(args.is_active || args.date) return Events.find({args}).populate(populateField).exec();
		if(args.city) return Events.find({'address.city':args.city}).populate(populateField).exec();
		if(args.tag) return Events.find({tags:{$in:[args.tag]}}).populate(populateField).exec();
		return Events.find().populate(populateField).exec();
	},
  
	getEvent: (root, args) => {
		return Events.findOne({_id:args.id}).exec();
	}
};